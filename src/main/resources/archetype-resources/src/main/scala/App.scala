package ${package}

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.StructType

object App
{
	def main(args: Array[String]): Unit =
	{
		val spark: SparkSession = SparkSession.builder.master("local[*]").appName("Stateful Structured Streaming").getOrCreate()
		
		spark.sparkContext.setLogLevel("WARN")
		
		val rows = java.util.Arrays.asList(Row("Bob", "Whiteklay", 32, "Developer"),
			Row("Jennifer", "Paper Co", 29, "Paper pusher"),
			Row("Steve", "SHIELD", 98, "Captain"))
		
		val schema = new StructType().add("name","string").add("company","string").add("age", "integer").add("profession", "string")
		
		
		val df = spark.createDataFrame(rows, schema)
		
		df.show
		
	}
}