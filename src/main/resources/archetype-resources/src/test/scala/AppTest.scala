package ${package}

import org.scalatest.FunSuite
import com.holdenkarau.spark.testing.{DatasetSuiteBase, SharedSparkContext}
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{Row, SparkSession}

class AppTest extends FunSuite with SharedSparkContext with DatasetSuiteBase
{
	test("sample test")
	{
		val spark = SparkSession.builder().master("local[*]").getOrCreate()
		
		spark.sparkContext.setLogLevel("WARN")
		
		val rows = java.util.Arrays.asList(Row("Bob", "Whiteklay", 32, "Developer"),
			Row("Jennifer", "Paper Co", 29, "Paper pusher"),
			Row("Steve", "SHIELD", 98, "Captain"))
		
		val schema = new StructType().add("name","string").add("company","string").add("age", "integer").add("profession", "string")
		
		
		val df = spark.createDataFrame(rows, schema)
		
		assertDatasetEquals(df,df)
	}
}
